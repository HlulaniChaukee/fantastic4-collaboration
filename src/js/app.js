angular.module('app', []);

angular.module('app').controller("MainController", function(){
    var add = this;
    add.title = 'Fantastic4 Survey';
    add.searchInput = '';
    add.surveys = [
        {
			id : 1,
            title: 'Drungs abuse',
			description: 'What is your say about drugs abuse',
			month: 03,
            year: 2011,
			instruction: 'Collect and analyse data to solve the problem',
			insComment: 'Talk to people',
			insOrder: 'Where to start',
			question: 'Can you solve the problem',
			queComment: 'State how',
			queOrder: 'Where to start',
            favorite: true
        },
        {
			id: 2,
            title: 'Child abuse',
			description: 'What is your say about child abuse',
			month: 07,
            year: 2010,
			instruction: 'Collect and analyse data to solve the problem',
			insComment: 'Talk to people',
			insOrder: 'Where to start',
			question: 'Can you solve the problem',
			queOrder: 'Where to start',
			queComment: 'State how',
            favorite: false
        },
        {
			id:3,
            title: 'Crime in South Africa',
			description: 'What is your say about crime in South Africa',
			month: 01,
            year: 2002,			
			instruction: 'Collect and analyse data to solve the problem',
			insComment: 'Talk to people',
			insOrder: 'Where to start',
			question: 'Can you solve the problem',
			queComment: 'State how',
			queOrder: 'Where to start',
            favorite: true
        }
    ];
 
    add.orders = [
        {
            id: 1,
            title: 'Year Ascending',
            key: 'year',
            reverse: false
        },
        {
            id: 2,
            title: 'Year Descending',
            key: 'year',
            reverse: true
        },
		 {
            id: 2,
            title: 'month Ascending',
            key: 'month',
            reverse: false
        },
		 {
            id: 2,
            title: 'month Descending',
            key: 'month',
            reverse: true
        }
    ];
	
add.questions = [];
add.instructions = [];
    add.order = add.orders[0];
    add.new = {};
    add.addShow = function() {
			
		add.instructions.push(add.new);
        add.questions.push(add.new);
		add.surveys.push(add.new);
        add.new = {};
    };
});