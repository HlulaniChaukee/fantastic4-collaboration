#Responsibilities for scrumMasters

##Coach

SrumMaster can be a coach for the Scrum team-both the development team and the product owner.

##Servant Leader

The ScrumMaster is first and foremost a servant to the Scrum team, ensuring that its highest-priority needs are being met.

##Process Authority

The ScrumMaster is the Scrum team����s process authority. In this capacity, the ScrumMaster is empowered to ensure that the Scrum team enacts and adheres to the Scrum values, principles, and practices along with the Scrum team����s specific approaches

##Interference Shield

The ScrumMaster protects the development team from outside interference so that it can remain focused on delivering business value every sprint. Interference can come from any number of sources, from managers who want to redirect team members in the middle of a sprint, to issues originating from other teams.

##Impediment Remover

The ScrumMaster also takes responsibility for removing impediments that inhibit the team's productivity (when the team members themselves cannot reasonably remove them).
For example, I observed a Scrum team that was consistently unable to meet its sprint goals. The impediment was unstable production servers that the team used during testing (as part of its definition of done).

##Change Agent

The ScrumMaster must help change more than faulty servers and similar impediments.
A good ScrumMaster must help change minds as well.
Scrum can be very disruptive to the status quo; the change that is required to be successful with Scrum can be difficult.
