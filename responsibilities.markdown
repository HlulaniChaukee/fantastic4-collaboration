
#**Responsibilities of a scrum master**
---------------------------------------
---------------------------------------

* *Ensures that the team is fully functional and productive*
* *Ensures that the process is followed, including issuing invitations to daily scrums, sprint reviews, and sprint planningHelps foster scrum practice*

